package data

type DataOptions struct {
	StartingResourcesOptions  []string
	NationsOptions            []string
	UnitTypesOptions          []string
	SeasonOptions             []string
	MapShapeOptions           []string
	TerrainTypeOptions        []string
	MineralsOptions           []string
	MapSizeOptions            []string
	BallonsOptions            []string
	CannonsOptions            []string
	PeaceTimeOptions          []string
	CenturyOptions            []string
	CaptureOptions            []string
	DipCenterAndMarketOptions []string
	AlliesOptions             []string
	LimitOfPopulationOptions  []string
	WallsOptions              []string
}

type Setup struct {
	MapSetup     MapSetup
	PlayersSetup []PlayerSetup
}

type PlayerSetup struct {
	Nation   string
	UnitType string
}

type MapSetup struct {
	StartingResources  string
	Season             string
	MapShape           string
	TerrainType        string
	Minerals           string
	MapSize            string
	Ballons            string
	Cannons            string
	PeaceTime          string
	Century            string
	Capture            string
	DipCenterAndMarket string
	Allies             string
	LimitOfPopulation  string
	Walls              string
}

package utils

const (
	BotPrefix          string = "!cgr"
	TokenKey           string = "TOKEN"
	MaxNumberOfPlayers int    = 8
)

func GetDataFilePath() string {
	return "assets/data.json"
}

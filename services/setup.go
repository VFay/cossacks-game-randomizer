package services

import (
	"encoding/json"
	"math/rand"
	"os"
	"strconv"
	"strings"

	"gitlab.com/VFay/cossacks-game-randomizer/data"
	"gitlab.com/VFay/cossacks-game-randomizer/utils"
)

type SetupService struct {
	startingResourcesOptions  []string
	nationsOptions            []string
	unitTypesOptions          []string
	seasonOptions             []string
	mapShapeOptions           []string
	terrainTypeOptions        []string
	mineralsOptions           []string
	mapSizeOptions            []string
	ballonsOptions            []string
	cannonsOptions            []string
	peaceTimeOptions          []string
	centuryOptions            []string
	captureOptions            []string
	dipCenterAndMarketOptions []string
	alliesOptions             []string
	limitOfPopulationOptions  []string
	wallsOptions              []string
}

func (s *SetupService) Init() {
	file, _ := os.ReadFile(utils.GetDataFilePath())

	data := data.DataOptions{}
	_ = json.Unmarshal([]byte(file), &data)

	s.startingResourcesOptions = data.StartingResourcesOptions
	s.nationsOptions = data.NationsOptions
	s.unitTypesOptions = data.UnitTypesOptions
	s.seasonOptions = data.SeasonOptions
	s.mapShapeOptions = data.MapShapeOptions
	s.terrainTypeOptions = data.TerrainTypeOptions
	s.mineralsOptions = data.MineralsOptions
	s.mapSizeOptions = data.MapSizeOptions
	s.ballonsOptions = data.BallonsOptions
	s.cannonsOptions = data.CannonsOptions
	s.peaceTimeOptions = data.PeaceTimeOptions
	s.centuryOptions = data.CenturyOptions
	s.captureOptions = data.CaptureOptions
	s.dipCenterAndMarketOptions = data.DipCenterAndMarketOptions
	s.alliesOptions = data.AlliesOptions
	s.limitOfPopulationOptions = data.LimitOfPopulationOptions
	s.wallsOptions = data.WallsOptions
}

func (s *SetupService) GenerateGameSetup(playersNumber int) string {
	var playerSetups []data.PlayerSetup

	for i := 0; i < playersNumber; i++ {
		randNationIndex := rand.Intn(len(s.nationsOptions))
		randUnitTypesIndex := rand.Intn(len(s.unitTypesOptions))

		playerSetup := data.PlayerSetup{
			Nation:   s.nationsOptions[randNationIndex],
			UnitType: s.unitTypesOptions[randUnitTypesIndex],
		}

		playerSetups = append(playerSetups, playerSetup)
	}

	randStartingIndex := rand.Intn(len(s.startingResourcesOptions))
	randSeasonIndex := rand.Intn(len(s.seasonOptions))
	randMapShapeIndex := rand.Intn(len(s.mapShapeOptions))

	terrainTypeIndex := rand.Intn(len(s.terrainTypeOptions))
	mineralsIndex := rand.Intn(len(s.mineralsOptions))
	mapSize := rand.Intn(len(s.mapSizeOptions))
	ballonsIndex := rand.Intn(len(s.ballonsOptions))
	cannonsIndex := rand.Intn(len(s.cannonsOptions))
	peaceTimeIndex := rand.Intn(len(s.peaceTimeOptions))
	centuryIndex := rand.Intn(len(s.centuryOptions))
	captureIndex := rand.Intn(len(s.captureOptions))
	dipCenterAndMarketIndex := rand.Intn(len(s.dipCenterAndMarketOptions))
	alliesIndex := rand.Intn(len(s.alliesOptions))
	limitOfPopulationIndex := rand.Intn(len(s.limitOfPopulationOptions))
	wallsIndex := rand.Intn(len(s.wallsOptions))

	gameSetup := data.Setup{
		MapSetup: data.MapSetup{
			StartingResources:  s.startingResourcesOptions[randStartingIndex],
			Season:             s.seasonOptions[randSeasonIndex],
			MapShape:           s.mapShapeOptions[randMapShapeIndex],
			TerrainType:        s.terrainTypeOptions[terrainTypeIndex],
			Minerals:           s.mineralsOptions[mineralsIndex],
			MapSize:            s.mapSizeOptions[mapSize],
			Ballons:            s.ballonsOptions[ballonsIndex],
			Cannons:            s.cannonsOptions[cannonsIndex],
			PeaceTime:          s.peaceTimeOptions[peaceTimeIndex],
			Century:            s.centuryOptions[centuryIndex],
			Capture:            s.captureOptions[captureIndex],
			DipCenterAndMarket: s.dipCenterAndMarketOptions[dipCenterAndMarketIndex],
			Allies:             s.alliesOptions[alliesIndex],
			LimitOfPopulation:  s.limitOfPopulationOptions[limitOfPopulationIndex],
			Walls:              s.wallsOptions[wallsIndex],
		},
		PlayersSetup: playerSetups,
	}

	return s.FormatSetupToString(gameSetup)
}

func (s *SetupService) FormatSetupToString(setup data.Setup) string {
	builder := strings.Builder{}

	builder.WriteString("Season: " + setup.MapSetup.Season + "\n")
	builder.WriteString("Map Shape: " + setup.MapSetup.MapShape + "\n")
	builder.WriteString("Terrain Type: " + setup.MapSetup.TerrainType + "\n")
	builder.WriteString("Starting Resources: " + setup.MapSetup.StartingResources + "\n")
	builder.WriteString("Minerals: " + setup.MapSetup.Minerals + "\n")
	builder.WriteString("Map Size: " + setup.MapSetup.MapSize + "\n")
	builder.WriteString("Ballons: " + setup.MapSetup.Ballons + "\n")
	builder.WriteString("Cannons: " + setup.MapSetup.Cannons + "\n")
	builder.WriteString("Peace Time: " + setup.MapSetup.PeaceTime + "\n")
	builder.WriteString("Century: " + setup.MapSetup.Century + "\n")
	builder.WriteString("Capture: " + setup.MapSetup.Capture + "\n")
	builder.WriteString("Dip Center and Market: " + setup.MapSetup.DipCenterAndMarket + "\n")
	builder.WriteString("Allies: " + setup.MapSetup.Allies + "\n")
	builder.WriteString("Limit of population: " + setup.MapSetup.LimitOfPopulation + "\n")
	builder.WriteString("Walls: " + setup.MapSetup.Walls + "\n")

	builder.WriteString("\n")

	for i, s := range setup.PlayersSetup {
		builder.WriteString("Player " + strconv.Itoa(i+1) + "\n")
		builder.WriteString("Nation: " + s.Nation + "\n")
		builder.WriteString("Unit Type: " + s.UnitType + "\n")
		builder.WriteString("\n")
	}

	return builder.String()
}
